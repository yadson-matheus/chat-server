const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const {srvSecurity} = require('../configs/config');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
        trim: true,
    },
    email: {
        type: String,
        unique: true,
        uniqueCaseInsensitive: true,
        required: [true, 'e-mail is required'],
        lowercase: true,
        trim: true,
    },
    password: {
        type: String,
        required: [true, 'password is required'],
        select: false,
    },
    friends: [{
        requestAccepted: {
            type: Boolean,
            default: false,
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
    }],
}, {
    timestamps: true,
    autoCreate: true,
});

UserSchema.pre('save', async function(next) {
    if (!this.isModified('password')) next();

    this.password = await bcrypt.hash(this.password, 10);
});

UserSchema.statics.searchByFilter = async function(filter, cb) {
    const {search, limit} = filter;
    const pattern = new RegExp(`.*${search}.*`, 'i');

    return this.find({
        $or: [
            {name: {$regex: pattern}},
            {email: {$regex: pattern}},
        ],
    })
    .select(['name', 'email'])
    .limit(limit)
    .exec(cb);
};

UserSchema.methods.compareHash = async function(hash) {
    return await bcrypt.compare(hash, this.password);
};

UserSchema.methods.generateToken = function() {
    const {salt, expiration} = srvSecurity;

    return jwt.sign({id: this.id}, salt, {expiresIn: expiration});
};

UserSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', UserSchema);
