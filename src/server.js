const app = require('express')();
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');

const {dbHost, dbPort, dbUser, dbPassword, dbName, dbOptions} = require('./configs/database');
const {srvHost, srvPort, srvPath} = require('./configs/config');
const routes = require('./routes');

try {
    const dbAuth = dbUser ? `${dbUser}:${dbPassword}@` : '';

    mongoose.connect(`mongodb://${dbAuth + dbHost}:${dbPort}/${dbName}`, dbOptions);
}
catch (err) {
    console.log('database connection failed');
}

app.use(cors());
app.use(bodyParser.json());
app.use(srvPath, routes);

app.listen(srvPort, srvHost, () => {
    console.log(`server running at http://${srvHost}:${srvPort + srvPath}`);
});
