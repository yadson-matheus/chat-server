const jwt = require('jsonwebtoken');

const {srvSecurity} = require('../configs/config');

module.exports = (req, res, next) => {
    const authorization = req.headers.authorization;

    if (!authorization)
        return res.status(401).json({error: 'no token provided'});

    const parts = authorization.split(' ');

    if (!parts.length === 2)
        return res.status(401).json({error: 'token error'});

    const [scheme, token] = parts;

    if (!/^Bearer$/i.test(scheme))
        return res.status(401).json({error: 'invalid token format'});

    jwt.verify(token, srvSecurity.salt, (err, decoded) => {
        if (err)
            return res.status(401).json({error: 'invalid token'});

        req.auth = {
            id: decoded.id,
            role: decoded.role,
        };

        next();
    });
};
