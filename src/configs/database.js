module.exports = {
    dbHost: 'localhost',
    dbPort: 27017,
    dbUser: '',
    dbPassword: '',
    dbName: 'we-chat',
    dbOptions: {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
};
