module.exports = {
    srvHost: 'localhost',
    srvPort: 3001,
    srvPath: '/api',
    srvSecurity: {
        salt: 'secret',
        expiration: 86400,
    },
};
