const express = require('express');

const authMiddleware = require('./middlewares/auth');

const UserController = require('./controllers/UserController');

const routes = express.Router();

routes.post('/user/login', UserController.login);
routes.post('/user/create', UserController.add);
routes.get('/user/search', authMiddleware, UserController.search);

module.exports = routes;
