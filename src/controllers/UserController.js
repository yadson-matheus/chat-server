const yup = require('yup');

const User = require('../models/User');

module.exports = {
    async add(req, res) {
        const userSchema = yup.object().shape({
            name: yup.string().required().trim(),
            email: yup.string().email().required(),
            password: yup.string().required(),
        });

        try {
            const userData = await userSchema.validate(req.body);

            const user = await User.create(userData);

            user.password = undefined;

            const token = user.generateToken();

            return res.status(200).json({user, token});
        }
        catch (err) {
            return res.status(400).json({error: 'registration failed'});
        }
    },

    async login(req, res) {
        const userSchema = yup.object().shape({
            email: yup.string().email().required(),
            password: yup.string().required(),
        });

        try {
            const {email, password} = await userSchema.validate(req.body);

            const user = await User.findOne({email}).select('+password');

            if (!user)
                return res.status(400).json({error: 'user not found'});

            if (!await user.compareHash(password))
                return res.status(400).json({error: 'invalid password'});

            user.password = undefined;

            const token = user.generateToken();

            return res.status(200).json({user, token});
        }
        catch (err) {
            return res.status(400).json({error: 'authentication failed'});
        }
    },

    async search(req, res) {
        const filterSchema = yup.object().shape({
            search: yup.string().required().trim(),
            limit: yup.number().oneOf([10, 30, 50]).default(10),
        });

        try {
            const filterData = await filterSchema.validate(req.query);

            const users = await User.searchByFilter(filterData);

            if (users.length === 0)
                return res.status(400).json({error: 'nothing found'});

            return res.status(200).json({users});
        }
        catch (err) {
            return res.status(400).json({error: 'search failed'});
        }
    },
};
